var airportName = getUrlVars()["airportName"];
var dbCreated = getUrlVars()["dbCreated"];
var db;

onDeviceReady();
function onDeviceReady() {
	$("#todayDateLabe").append(getTodayDate());
}

$(document).ready(function(){
	$("#flightListButton").click(function(){
		window.location.href="index.html?dbCreated="+dbCreated;
    });
	$("#confirmcancelAddNewFlightButton").click(function(){
		window.location.href="index.html?dbCreated="+dbCreated;
    });
	$("#setStartTimeButton").click(function(){
		getTime(0);
		$("#setStartTimeButton").hide();
		$("#startTimePanel").hide();
	});
	$("#setDisconnectionTimeButton").click(function(){
		getTime(1);
		$("#setDisconnectionTimeButton").hide();
		$("#disconnectionTimePanel").hide();
	});
	$("#setLandingPlaneTimeButton").click(function(){
		getTime(2);
		$("#setLandingPlaneTimeButton").hide();
		$("#landingPlaneTimePanel").hide();
	});
	$("#setLandingGliderTimeButton").click(function(){
		getTime(3);		
		$("#landingGliderTimePanel").hide();
	});
	$("#winchRadio").click(function(){
		$('#planePilotNameCell').hide();
		$('#planePilotNameForm').val('');
		$('#landingPlaneTimeCell').hide();
		$('#landingPlaneTimeForm').val('');
		$('#planeFlightTimePanel').hide();
		$('#planeHourTimeForm').val('');		
		$('#planeMinTimeForm').val('');	
		$('#planeOnEarthTimeForm').val('');
		$('#planeTypeCell').hide();
		$('#planeTypeForm').val('');
		$('#startModeForm').val('W');
	});
	$("#planeRadio").click(function(){
		$('#planePilotNameCell').show();
		$('#landingPlaneTimeCell').show();
		$('#planeFlightTimeCell').show();
		$('#planeHourTimeForm').val('');		
		$('#planeMinTimeForm').val('');	
		$('#planeOnEarthTimeForm').val('');
		$('#planeTypeCell').show();
		$('#startModeForm').val('S');
	});
	$("#saveChangesButton").click(function(){
		getDate();
		db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
		db.transaction(addNewFlight, transaction_error);
		window.location.href="index.html?dbCreated="+dbCreated;
	});
})

function transaction_error(tx, error) {
    console.log("[Dodaj nowy] Błąd bazy danych " + error);
}

function addNewFlight(tx){
	if(document.getElementById("landingGliderTimeForm").value=='')
		status = 'W powietrzu';
	else
		status = 'Zakonczony';
	
	if(document.getElementById("startTimeForm").value!='' && document.getElementById("landingGliderTimeForm").value!=''){	
				document.getElementById("gliderHourTimeForm").value = timeDifference(startTimeForm.value,landingGliderTimeForm.value,'H');
				document.getElementById("gliderMinTimeForm").value = timeDifference(startTimeForm.value,landingGliderTimeForm.value,'M');
			}
			if(document.getElementById("startTimeForm").value!='' && document.getElementById("landingPlaneTimeForm").value!=''){	
				document.getElementById("planeHourTimeForm").value = timeDifference(startTimeForm.value,landingPlaneTimeForm.value,'H');
				document.getElementById("planeMinTimeForm").value = timeDifference(startTimeForm.value,landingPlaneTimeForm.value,'M');
			}

	var addQuery = "INSERT INTO flights (date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, status, comments) " +
			  "VALUES ('"+document.getElementById("dateForm").value +"','"+ document.getElementById("pilotNameForm").value +"','"+document.getElementById("planePilotNameForm").value+"','"+ document.getElementById("passengerNameForm").value +"','"+ document.getElementById("taskNumberForm").value +"','"+ document.getElementById("exerciseNumberForm").value +"','"+ document.getElementById("startModeForm").value +"','"+ document.getElementById("startTimeForm").value +"','"+ document.getElementById("disconnectionTimeForm").value +"','"+ document.getElementById("landingGliderTimeForm").value +"','"+ document.getElementById("landingPlaneTimeForm").value +"','"+ document.getElementById("gliderHourTimeForm").value +"','"+ document.getElementById("gliderMinTimeForm").value +"','"+ document.getElementById("planeOnEarthTimeForm").value +"','"+ document.getElementById("planeHourTimeForm").value +"','"+ document.getElementById("planeMinTimeForm").value +"','"+ document.getElementById("gliderTypeForm").value +"','"+ document.getElementById("planeTypeForm").value +"','"+status+"','"+ document.getElementById("commentsForm").value +"')";
	tx.executeSql(addQuery);	
}

function getDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;

	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	today = dd+'.'+mm+'.'+yyyy;
	$('#dateForm').show();
	document.getElementById('dateForm').value = today;
}

function getTodayDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;

	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	today = dd+'.'+mm+'.'+yyyy;
	$('#dateForm').show();
	return today;
}

function getTime(field){
	var today = new Date();
	var hours  = today.getHours();
	var mins = today.getMinutes();
	var seconds = today.getSeconds();
	hoursString = hours.toString();
	minsString = mins.toString();
	secondsString = seconds.toString();
	if(hoursString.length==1)
		hoursString='0'+hoursString;
	if(minsString.length==1)
		minsString='0'+minsString;
	if(secondsString.length==1)
		secondsString='0'+secondsString;
	
	time = hoursString+':'+minsString+':'+secondsString;
	
	if(field==0){
		document.getElementById('startTimeForm').value = time;
		$('#startTimeLabel').append(time);
	}
	if(field==1){
		document.getElementById('disconnectionTimeForm').value = time;
		$('#disconnectionTimeLabel').append(time);
	}
	if(field==2){
		document.getElementById('landingPlaneTimeForm').value = time;
		$('#landingPlaneTimeLabel').append(time);
	}
	if(field==3){
		document.getElementById('landingGliderTimeForm').value = time;
		$('#landingGliderTimeLabel').append(time);
	}
}

function timeDifference(time1, time2, timePart) {
   var parts = time1.split(':');
   var date1 = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
   parts = time2.split(':');
   var date2 = new Date(new Date(0, 0, 0, parts[0], parts[1], parts[2]) - date1);

	if(timePart=='H')
		return (date2.getHours()-1);
	if(timePart=='M')
		return (date2.getMinutes());
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}