document.addEventListener('deviceready', onDeviceReady, false);
var date = getUrlVars()["date"];
var gliderType= getUrlVars()["gliderType"];

$(document).ready(function(){
	$("#flightListButton").click(function(){
      window.location.href="index.html";
});
})


function onDeviceReady() {
	
	if(gliderType!='')
		window.sql ="SELECT * " +
			  "FROM flights " +
			  "WHERE date='"+date+"' "+
			  "AND gliderType='"+gliderType+"' " +
			  "ORDER BY id ASC";
			 else
				 window.sql = "SELECT * " +
			  "FROM flights " +
			  "WHERE date='"+date+"' "+
			  "ORDER BY id ASC";
	db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
	db.transaction(getFlights);
	
	
	function getFlights(tx){
	
			  tx.executeSql(window.sql, [], getFlights_success);
	}
	
	
	function getFlights_success(tx,results){ 
		window.results = results;
		var len = results.rows.length;
		//alert("Len: "+len);
		if(len==0){
			$('#exportInfoAlert').append('<strong>Brak lotów spełniających podane kryteria!</strong>');
			$('#exportInfoAlert').addClass("alert-danger");
			$('#exportInfoAlert').show();
			return
		}
		
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFSWin, getDirectoryFail);
	
	function onFSWin(fileSystem) {
		fileSystem.root.getDirectory("Loty", {create: true, exclusive: false}, getDirectorySuccess, getDirectoryFail);
	}

	function getDirectorySuccess(parent) {
	}

	function getDirectoryFail(error) {
	}

	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFSWin2, onFSFail2);

	function onFSWin2(fileSystem) {
		var dateToFileName=date.substr(6,4)+'.'+date.substr(3,2)+'.'+date.substr(0,2);
		if(gliderType!='')
			fileSystem.root.getFile("Loty/"+dateToFileName+"_"+gliderType+".csv", {create: true, exclusive: false}, gotFileEntry, onFSFail2);
		else
			fileSystem.root.getFile("Loty/"+dateToFileName+".csv", {create: true, exclusive: false}, gotFileEntry, onFSFail2);
	}

	function gotFileEntry(fileEntry) {
		fileEntry.createWriter(gotFileWriter, onFSFail2);
	}

	function gotFileWriter(writer) {
		
		
		
		window.results = results;
		var len = results.rows.length;
			if(gliderType!='')
				$('#exportInfoAlert').append('<strong>Wyeksportowano dane do pliku <b>/Loty/'+date+'_'+gliderType+'.csv</b></strong>');
			else
				$('#exportInfoAlert').append('<strong>Wyeksportowano dane do pliku <b>/Loty/'+date+'.csv</b></strong>');
			$('#exportInfoAlert').addClass("alert-success");
			$('#exportInfoAlert').show();
		
		var generateCSV = '';
		for (var i=0; i<len; i++) {
			var flight = results.rows.item(i);
			generateCSV += flight.date+";"+flight.pilotName+";"+flight.passengerName+";"+flight.planePilotName+";"+flight.taskNumber+";"+flight.exerciseNumber+";"+flight.startTime+";"+flight.disconnectionTime+";"+flight.landingGliderTime+";"+flight.landingPlaneTime+";"+flight.gliderHourTime+";"+flight.gliderMinTime+";"+flight.planeOnEarthTime+";"+flight.planeHourTime+";"+flight.planeMinTime+";"+flight.gliderType+";"+flight.planeType+";"+flight.comments+"\n";
	
		}

		writer.onwrite = function(evt) {
				console.log("write success");
			};
		writer.seek(writer.length);
		writer.write(generateCSV);
		
		
	}

	function onFSFail2(error) {
		console.log(error.code);
	}
}
}


function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

