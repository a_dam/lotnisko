var airportName = 'EPZP';
var databasePassword = 'lotyAZL';
var db;
var dbCreated = false;
var tabCreated = false;
var tab = 0;
var showMode;
var searchSentence = '';
onDeviceReady();

function onDeviceReady() {
	airportName = "EPZP";
	databasePassword = 'lotyAZL';
	var dbCreated = window.localStorage.getItem("testowyKlucz");
	var airportName = window.localStorage.getItem("airportName");
	databasePassword = window.localStorage.getItem("databasePassword");
	//alert("Z zapisanego: "+databasePassword);
	showMode = window.localStorage.getItem("showMode");
	if(airportName==null)
		airportName = "EPZP";
	if(databasePassword==null)
		databasePassword = "lotyAZL";
	if(showMode==null)
		showMode = "Dzisiejsze";
    db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
	$("#title").text(airportName);
	$("#airportNameForm").val(airportName);
	//$("#setDatabasePasswordForm").val(databasePassword);
	$("#showModeForm").attr("placeholder", "Pokaż loty: "+showMode);
	dbCreated2=getUrlVars()["dbCreated"];
	if(dbCreated2){
		dbCreated=getUrlVars()["dbCreated"];	
	}
    if (dbCreated)
		db.transaction(getFlights, transaction_error);
    else
    	db.transaction(populateDB, transaction_error, populateDB_success);
	db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
	db.transaction(getNames, transaction_error);
	db.transaction(getPlanes, transaction_error);	
	db.transaction(getGliders, transaction_error);
}

$(document).ready(function(){
	$("#allButton").click(function(){
        tab = 0;	
		onDeviceReady();
		$("#allButton").addClass("btn-primary");
		$("#finishedButton,#inAirButton,#winchButton,#planeButton,#searchButton").removeClass("btn-primary");
		$("#planeTypeHeadCell,#planePilotNameHeadCell,#landingPlaneTimeHeadCell,#planeCategoryHeadCell,#planeHourHeadCell,#planeMinHeadCell,#planeOnEarthHeadCell").show();
		$("#teamHeadCell").attr("colspan", "3");
		$("#timeHeadCell").attr("colspan", "4");
		$("#flightTimeHeadCell").attr("colspan", "5");
    });
    $("#inAirButton").click(function(){
		tab = 1;	
		onDeviceReady();
		$("#inAirButton").addClass("btn-primary");
		$("#allButton").addClass("btn-default");
		$("#finishedButton,#allButton,#winchButton,#planeButton,#searchButton").removeClass("btn-primary");
		$("#planeTypeHeadCell,#planePilotNameHeadCell,#landingPlaneTimeHeadCell,#planeCategoryHeadCell,#planeHourHeadCell,#planeMinHeadCell,#planeOnEarthHeadCell").show();
		$("#teamHeadCell").attr("colspan", "3");
		$("#timeHeadCell").attr("colspan", "4");
		$("#flightTimeHeadCell").attr("colspan", "5");
    });
	$("#finishedButton").click(function(){
		tab = 2;	
		onDeviceReady();
		$("#finishedButton").addClass("btn-primary");
		$("#allButton").addClass("btn-default");
		$("#inAirButton,#allButton,#winchButton,#planeButton,#searchButton").removeClass("btn-primary");
		$("#planeTypeHeadCell,#planePilotNameHeadCell,#landingPlaneTimeHeadCell,#planeCategoryHeadCell,#planeHourHeadCell,#planeMinHeadCell,#planeOnEarthHeadCell").show();
		$("#teamHeadCell").attr("colspan", "3");
		$("#timeHeadCell").attr("colspan", "4");
		$("#flightTimeHeadCell").attr("colspan", "5");
	});
	$("#winchButton").click(function(){
		tab = 3;	
		onDeviceReady();
		$("#winchButton").addClass("btn-primary");
		$("#allButton").addClass("btn-default");
		$("#inAirButton,#allButton,#finishedButton,#planeButton,#searchButton").removeClass("btn-primary");
		$("#planeTypeHeadCell,#planePilotNameHeadCell,#landingPlaneTimeHeadCell,#planeCategoryHeadCell,#planeHourHeadCell,#planeMinHeadCell,#planeOnEarthHeadCell").hide();
		$("#teamHeadCell").attr("colspan", "2");
		$("#timeHeadCell").attr("colspan", "3");
		$("#flightTimeHeadCell").attr("colspan", "2");
		//$("#flightsList").remove("id", "");
		$("[id*=planePilotNameCell]").hide();
		$("[id*=landingPlaneTimeCell]").hide();
		//$('#flightsList').remove("planePilotNameCell");
    });
    $("#planeButton").click(function(){
		tab = 4;	
		onDeviceReady();
		$("#planeButton").addClass("btn-primary");
		$("#allButton").addClass("btn-default");
		$("#inAirButton,#allButton,#finishedButton,#winchButton,#searchButton").removeClass("btn-primary");
		$("#planeTypeHeadCell,#planePilotNameHeadCell,#landingPlaneTimeHeadCell,#planeCategoryHeadCell,#planeHourHeadCell,#planeMinHeadCell,#planeOnEarthHeadCell").show();
		$("#teamHeadCell").attr("colspan", "3");
		$("#timeHeadCell").attr("colspan", "4");
		$("#flightTimeHeadCell").attr("colspan", "5");
    });
	$("#setQuery").click(function(){
		searchSentence = document.getElementById('queryForm').value;
		$('#searchModal').modal('hide');
		tab = 5;
		onDeviceReady();
		$("#searchButton").addClass("btn-primary");
		$("#allButton").addClass("btn-default");
		$("#inAirButton,#allButton,#finishedButton,#planeButton,#winchButton").removeClass("btn-primary");
		$("#planeTypeHeadCell,#planePilotNameHeadCell,#landingPlaneTimeHeadCell,#planeCategoryHeadCell,#planeHourHeadCell,#planeMinHeadCell,#planeOnEarthHeadCell").show();
		$("#teamHeadCell").attr("colspan", "3");
		$("#timeHeadCell").attr("colspan", "4");
		$("#flightTimeHeadCell").attr("colspan", "5");
    });
	$("#setAirportName").click(function(){
		airportName = document.getElementById('airportNameForm').value;
		window.localStorage.setItem("airportName", airportName);
		$('#settingsModal').modal('hide');
		onDeviceReady();
    });
	$("#setDatabasePassword").click(function(){

		//alert("DB po zmianie: "+databasePassword);
		
		if(document.getElementById('currentPasswordForm').value==databasePassword){
			//alert("Takie jak haslo");
			if(document.getElementById('setDatabasePasswordForm').value==document.getElementById('currentAgainPasswordForm').value){
				databasePassword = document.getElementById('setDatabasePasswordForm').value;
				window.localStorage.setItem("databasePassword", databasePassword);
				$('#newPasswordModal').modal('hide');
			}else{
				$('#alertText').text("Hasła nie zgadzają się!");
				$('#othersPasswordsErrorAlert').show();
			}
		}
		else{
			$('#alertText').text("Błędne aktualne hasło.");
			$('#othersPasswordsErrorAlert').show();
		}
    });
	$("#showNewPasswordModal").click(function(){	
		$('#settingsModal').modal('hide');
		$('#newPasswordModal').modal('show');
		$('#othersPasswordsErrorAlert').hide();
		$('#alertText').text("");
		$('#currentPasswordForm').val("");
		$('#currentAgainPasswordForm').val("");
		$('#setDatabasePasswordForm').val("");
	});
	$("#addNewFlightButton").click(function(){
		db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
		db.transaction(addNewFlight, transaction_error);
		location.reload();
    });
	$("#exportButton").click(function(){
		$('#exportDateForm').val(getTodayDate());
    });
	$("#confirmExportButton").click(function(){
		window.location.href="export.html?date="+$('#exportDateForm').val()+"&gliderType="+$('#exportGliderTypeForm').val();
    });
	
	$("#confirmRemoveFlights").click(function(){
		//alert("Wprowadzone hasło: "+$("#databasePasswordForm").val());
		//alert("Przy potwierdzaniu: "+databasePassword);
		if($("#databasePasswordForm").val()==databasePassword){
			db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
			db.transaction(removeFlights, transaction_error);
			$('#removeModal').modal('hide');
			location.reload();
			//alert("Czyszczenie bazy");
			$('#removeModal').modal('hide');
		}
		else {
			$('#passwordErrorAlert').show();
			//alert("Niepoprawne hasło");
		}
	});
	$("#todayFlightsButton").click(function(){
		$('#showModeForm').val("Dzisiejsze");
		showMode = document.getElementById('showModeForm').value;
		window.localStorage.setItem("showMode", showMode);
		$('#settingsModal').modal('hide');
		location.reload();
	});
	$("#allFlightsButton").click(function(){
		$('#showModeForm').val("Wszystkie");
		showMode = document.getElementById('showModeForm').value;
		window.localStorage.setItem("showMode", showMode);
		$('#settingsModal').modal('hide');
		location.reload();
	});
	$("input[id*='NameForm']").bind('focus', function(){ $(this).autocomplete("search"); } );
	$("input[id*='liderTypeForm']").bind('focus', function(){ $(this).autocomplete("search"); } );
	$("input[id*='planeTypeForm']").bind('focus', function(){ $(this).autocomplete("search"); } );
	$("#queryForm,#exportGliderTypeForm").bind('focus', function(){ $(this).autocomplete("search"); } );
	$("#allGlidersRadio").click(function(){
		$('#exportGliderTypeForm').hide();
		$('#exportGliderTypeLabel').hide();
	});
	$("#oneGliderRadio").click(function(){
		$('#exportGliderTypeForm').show();
		$('#exportGliderTypeLabel').show();
	});
	$("#closeAppButton").click(function(){
		navigator.app.exitApp();
		//window.close();
	});
	$("#refreshButton").click(function(){
		location.reload();
	});
})

function transaction_error(tx, error) {
    console.log("[Lista] Błąd bazy danych: " + error);
}

function populateDB_success() {
	dbCreated = true;
	window.localStorage.setItem("testowyKlucz", dbCreated);
    db.transaction(getFlights, transaction_error);
}

function autoWidth(field) {
    var x = document.getElementById(field);
    currentSize = parseInt(x.getAttribute("size"));
    newSize=currentSize+1;
    x.setAttribute("size",newSize);
}

function checkDate(field) {
	var dateReg = new RegExp('^[0-9]{2}.[0-9]{2}.[0-9]{4}$', 'gi');
	if(!dateReg.test(field.value)){
		return false;
	} else {
		return true;
	}
}

function getFlights(tx) {
	if(tab==0){
		if(showMode=='Dzisiejsze'){
				var sql = "SELECT * " +
			  "FROM flights " +
			  "WHERE date='"+getTodayDate()+"'" +
			  "ORDER BY id DESC";	
		}	
		else{
		var sql = "SELECT * " +
		  "FROM flights " +
		  "ORDER BY id DESC";
		}
	}			  
	if(tab==1){
		if(showMode=='Dzisiejsze'){
			var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE status='W powietrzu'" +
			  "AND date='"+getTodayDate()+"'" +
			  "ORDER BY id DESC";
		}
		else{
			var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE status='W powietrzu'" +
			  "ORDER BY id DESC";
		}
		//activeView(finishedButton);	
	}
	if(tab==2){
		if(showMode=='Dzisiejsze'){
			var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE status='Zakonczony'" +
			  "AND date='"+getTodayDate()+"'" +
			   "ORDER BY id DESC";
		}
		else{
			var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE status='Zakonczony'" +
			   "ORDER BY id DESC";
		}
	}
	if(tab==3){
		if(showMode=='Dzisiejsze'){
				var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE startMode='W'" +
			  "AND date='"+getTodayDate()+"'" +
			   "ORDER BY id DESC";
		}
		else{
				var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE startMode='W'" +
			   "ORDER BY id DESC";
		}
	}
	if(tab==4){
		if(showMode=='Dzisiejsze'){
				var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE startMode='S'" +
			  "AND date='"+getTodayDate()+"'" +
			   "ORDER BY id DESC";
		}
		else{
			var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE startMode='S'" +
			   "ORDER BY id DESC";
		}
	}
	if(tab==5){
		if(showMode=='Dzisiejsze'){
				var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE gliderType='"+searchSentence+"'" +
			  "AND date='"+getTodayDate()+"'" +
			  "ORDER BY id DESC";
		}
		else{
				var sql = "SELECT id, date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, comments, status " +
			  "FROM flights " +
			  "WHERE gliderType='"+searchSentence+"'" +
			  "ORDER BY id DESC";
		}
	}
	tx.executeSql(sql, [], getFlights_success);
}

function getTime(){
	var today = new Date();
	var hours  = today.getHours();
	var mins = today.getMinutes();
	var seconds = today.getSeconds();
	hoursString = hours.toString();
	minsString = mins.toString();
	secondsString = seconds.toString();
	if(hoursString.length==1)
		hoursString='0'+hoursString;
	if(minsString.length==1)
		minsString='0'+minsString;
	if(secondsString.length==1)
		secondsString='0'+secondsString;
	
	return time = hoursString+':'+minsString+':'+secondsString;
	//return time = "21:40:00";
}

function setTime(cell,flightID,timeOf,sT) {
	var id = $('#' + cell).parent().attr("id");
	var time = getTime();
	//alert(id);
	$('#' + id).append(time.substring(0,5));
	$('#' + cell).hide();
	window.timeOf = timeOf;
	window.time = time;
	window.flightID = flightID;
	if(sT!=null){
		var startTime = sT.toString();
		//alert(startTime.substring(0,5));
		if(startTime.substring(0,5)==time.substring(0,5)){
			newTime = startTime.split(':');
			
			if(newTime[1]=='59'){
				hours=parseInt(newTime[0]);
				hours=hours+1;
				//alert(hours);
				window.time=hours+":00:"+newTime[2];
			}
			else{
				mins=parseInt(newTime[1]);
				mins=mins+1;
				if(mins<10){
					mins="0"+mins;
				}
				//alert(mins);
				window.time=newTime[0]+":"+mins+":"+newTime[2];
			}
		}
	}
	db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
	db.transaction(editFlight, transaction_error);
	db.transaction(getFlight, transaction_error);
	
}

function getFlight_success(tx,results) {
	//alert(":)");
	window.results = results;
	var flight = results.rows.item(0);
	if(flight.startTime!='' && flight.landingGliderTime!=''){
		time1 = timeDifference(flight.startTime,flight.landingGliderTime,'H');
		time2 = timeDifference(flight.startTime,flight.landingGliderTime,'M');
		window.query = "UPDATE flights " +
			  "SET gliderHourTime='"+time1+"', gliderMinTime='"+time2+"', status='Zakonczony' "+
			"WHERE id="+flightID;
		db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
		db.transaction(getTimes, transaction_error);
	}
	if(flight.startTime!='' && flight.landingPlaneTime!=''){
		time1= timeDifference(flight.startTime,flight.landingPlaneTime,'H');
		time2= timeDifference(flight.startTime,flight.landingPlaneTime,'M');
		window.query = "UPDATE flights " +
			"SET planeHourTime='"+time1+"', planeMinTime='"+time2+"' "+
			"WHERE id="+flightID;
		db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
		db.transaction(getTimes, transaction_error);
	}
	if((flight.startTime!='' && flight.landingPlaneTime!='') && (flight.startTime!='' && flight.landingGliderTime!='')){
		time1= timeDifference(flight.startTime,flight.landingGliderTime,'H');
		time2= timeDifference(flight.startTime,flight.landingGliderTime,'M');
		time3= timeDifference(flight.startTime,flight.landingPlaneTime,'H');
		time4= timeDifference(flight.startTime,flight.landingPlaneTime,'M');
		window.query = "UPDATE flights " +
			"SET planeHourTime='"+time3+"', planeMinTime='"+time4+"', gliderHourTime='"+time1+"', gliderMinTime='"+time2+"', status='Zakonczony' "+
			"WHERE id="+flightID;
		db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
		db.transaction(getTimes, transaction_error);
	}
	location.reload();
}

function getTimes(tx) {
	tx.executeSql(window.query);
}

function timeDifference(time1, time2, timePart) {
   var parts = time1.split(':');
   var date1 = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
   parts = time2.split(':');
   var date2 = new Date(new Date(0, 0, 0, parts[0], parts[1], parts[2]) - date1);
  //alert("Minuty: "+date2.getMinutes());
   flightInSeconds=((date2.getHours()-1)*3600)+(date2.getMinutes()*60)+(date2.getSeconds());
	//alert("Lot w sekundach: "+flightInSeconds);
   //alert("Sekundy: "+date2.getSeconds());
   if(parseInt(flightInSeconds)<=60){
	   if(timePart=='H')
		return (date2.getHours()-1);
		if(timePart=='M'){
		return 1;
	}
   }
   else{
	   if(flightInSeconds%60<30){
		   if(timePart=='H')
			return (date2.getHours()-1);
			if(timePart=='M')
				return date2.getMinutes();
	   }
	   else{
		     if(timePart=='H')
			return (date2.getHours()-1);
			if(timePart=='M')
				return date2.getMinutes()+1;
	   }
	/*if(timePart=='H')
		return (date2.getHours()-1);
	if(timePart=='M'){
		return (date2.getMinutes());
	}*/
   }
}

function editFlight(tx){
	if(timeOf!='landingGliderTime')
		status = 'W powietrzu';
	else
		status = 'Zakonczony';
	
	var updateQuery = "UPDATE flights " +
			  "SET "+timeOf+"='"+time+"', status='"+status+"' "+
			  "WHERE id="+flightID;
	tx.executeSql(updateQuery);
}

function removeFlights(tx){
	var removeQuery = "DELETE FROM flights WHERE id>0";
	tx.executeSql(removeQuery);
}

function getNames(tx) {
	var getNamesQuery = "SELECT pilotName FROM flights UNION SELECT passengerName FROM flights UNION SELECT planePilotName FROM flights";
	tx.executeSql(getNamesQuery,[], getNames_success);
}

function getNames_success(tx,results) {
	window.results = results;
	var len = results.rows.length;
	window.names = [];	
	for (var i=0; i<len; i++) {
		var flight = results.rows.item(i);
		window.names.push(flight.pilotName);
	}
	$("input[id*='NameForm']").autocomplete({
		  source: window.names,
		  minLength: 0,
	});
}

function getGliders(tx) {
	var getGliders = "SELECT DISTINCT gliderType FROM flights";
	tx.executeSql(getGliders,[], getGliders_success);
}

function getGliders_success(tx,results) {
	window.results = results;
	var len = results.rows.length;
	window.gliders = [];	
	for (var i=0; i<len; i++) {
		var flight = results.rows.item(i);
		window.gliders.push(flight.gliderType);
	}
	$("input[id*='liderTypeForm']").autocomplete({
		  source: window.gliders,
		  minLength: 0
	});	
	$("#queryForm").autocomplete({
		  source: window.gliders,
		  minLength: 0
	});		
	$("#queryForm").autocomplete( "option", "appendTo", ".eventInsForm" );
	$("#exportGliderTypeForm").autocomplete({
		  source: window.gliders,
		  minLength: 0
	});		
	$("#exportGliderTypeForm").autocomplete( "option", "appendTo", ".eventInsExportForm");
}

function getPlanes(tx) {
	var getPlanes = "SELECT DISTINCT planeType FROM flights";
	tx.executeSql(getPlanes,[], getPlanes_success);
}

function getPlanes_success(tx,results) {
	window.results = results;
	var len = results.rows.length;
	window.planes = [];	
	for (var i=0; i<len; i++) {
		var flight = results.rows.item(i);
		window.planes.push(flight.planeType);
	}
	$("input[id*='planeTypeForm']").autocomplete({
		  source: window.planes,
		  minLength: 0
	});
}

function getTodayDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;

	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	today = yyyy+'.'+mm+'.'+dd;
	$('#dateForm').show();
	return today;
}

function addNewFlight(tx){
	var addQuery = "INSERT INTO flights (date, pilotName, planePilotName, passengerName, taskNumber, exerciseNumber, startMode, startTime, disconnectionTime, landingGliderTime, landingPlaneTime, gliderHourTime, gliderMinTime, planeOnEarthTime, planeHourTime, planeMinTime, gliderType, planeType, status, comments) " +
			  "VALUES ('"+getTodayDate()+"','','','','','','W','','','','','','','','','','','','W powietrzu','')";
	tx.executeSql(addQuery);	
}

function getFlight(tx){
	//alert("Pobieranie danych");
	var removeQuery = "SELECT * FROM flights WHERE id="+flightID;
	tx.executeSql(removeQuery,[], getFlight_success);
}

function getFlights_success(tx, results) {
	with(document.getElementById('flightsList')){
	while(childNodes.length>0){
		  removeChild(childNodes[0])
	  }
	}
    var len = results.rows.length;
	if (len==0){
		$('#noFlightsAlert').show();
		$('#flightsListHead').hide();
	}
	else{
		$('#noFlightsAlert').hide();
		$('#flightsListHead').show();
		for (var i=0; i<len; i++) {
			window.results = results;
			var flight = results.rows.item(i);
			if(tab==3){
			$('#flightsList').append('<tr class="info" style="font-weight: bold;"><td style="vertical-align:middle;"><a href="flightdetails.html?id=' + flight.id + '&airportName='+airportName+'&dbCreated='+dbCreated+'">' + 
			flight.date +'</a></td><td style="vertical-align:middle;" class="rowlink-skip" id="pilotNameCell'+flight.id+'"><input type="text" class="form-control" placeholder="Pilot" maxlength="50" size="1" style="margin-bottom:10px;" id="pilotNameForm'+flight.id+'" onkeyup="autoWidth(id)" onblur="makeQueryToUpdateFlight(id, \'pilotName\','+flight.id+',\'pilotNameCell'+flight.id+'\')">'+
			'</td><td style="vertical-align:middle;" class="rowlink-skip" id="passengerNameCell'+flight.id+'"><input type="text" class="form-control" placeholder="Instruktor" maxlength="50" style="margin-bottom:10px;" name="pass" id="passengerNameForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'passengerName\','+flight.id+',\'passengerNameCell'+flight.id+'\')">'+
			'</td>'+
			'<td style="vertical-align:middle;" class="rowlink-skip" id="exerciseNumberCell'+flight.id+'"><select class="form-control" id="exerciseNumberForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'exerciseNumber\','+flight.id+',\'exerciseNumberCell'+flight.id+'\')"><option>4 - Wstępny lot zapoznawczy</option><option>5 - Działanie układów sterowania</option><option>6 - Koordynowanie wykonywanie zakrętów ze średnim przechyleniem</option><option>7 - Lot po prostej</option><option>8 - Zakręty</option><option>9a - Lot na małej prędkości</option><option>9b - Przeciągnięcie</option><option>10a - Rozpoznanie korkociągu i zapobieganie wejściu  w korkociąg</option><option>10b - Korkociągi ustalone: wejście i  wyprowadzanie</option><option>11 - Metody startu</option><option>11a - Start za wyciągarką</option><option>11b - Start za samolotem holującym</option><option>11c - Start z własnym zespołem napędowym</option><option>11d - Start za samochodem holującym</option><option>11e - Start z lin gumowych</option><option>12 - Krąg, podejście do lądowania i  lądowanie</option><option>13 - Pierwszy samodzielny lot</option><option>14 - Głębokie zakręty</option><option>15 - Techniki szybowania</option><option>15a - Lot w prądzie termicznym</option><option>15b - Lot  żaglowy</option><option>15c - Lot falowy</option><option>16 - Lądowania w terenie przygodnym</option><option>17 - Lot nawigacyjny</option><option>17 - Lot nawigacyjny (samodzielny)</option><option>17a - Planowanie lotu</option><option>17b - Nawigacja w locie</option><option>17c - Techniki lotu nawigacyjnego</option><option>Inne</option></select>'+
			'</td><td style="vertical-align:middle;" class="rowlink-skip" id="startTimeCell'+flight.id+'"><a onclick="setTime(this.id,'+flight.id+',\'startTime\');" class="btn btn-info btn-lg" id="startTimeButton'+flight.id+'"  role="button"><span class="glyphicon glyphicon-time"></span></a>'+
			'</td><td style="vertical-align:middle;" class="rowlink-skip" id="disconnectionTimeCell'+flight.id+'"><a onclick="setTime(this.id,'+flight.id+',\'disconnectionTime\');" class="btn btn-danger btn-lg" id="disconnectionTimeButton'+flight.id+'"  role="button"><span class="glyphicon glyphicon-time"></span></a>'+
			'</td><td style="vertical-align:middle;" class="rowlink-skip" id="landingGliderTimeCell'+flight.id+'"><a onclick="setTime(this.id,'+flight.id+',\'landingGliderTime\','+flight.startTime+');" class="btn btn-primary btn-lg" id="landingGliderTimeButton'+flight.id+'" role="button"><span class="glyphicon glyphicon-time"></span></a>'+
			'</td><td style="vertical-align:middle;">'+
			flight.gliderHourTime+'</td><td style="vertical-align:middle;">'+
			flight.gliderMinTime+'</td><td style="vertical-align:middle;" class="rowlink-skip" id="gliderTypeCell'+flight.id+'"><input type="text" class="form-control gliderType" placeholder="Typ i oznaczenie szybowca" maxlength="15" style="margin-bottom:10px;" id="gliderTypeForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'gliderType\','+flight.id+',\'gliderTypeCell'+flight.id+'\')">'+
			'</td><td style="vertical-align:middle;" class="rowlink-skip" id="commentsCell'+flight.id+'"><input type="text" class="form-control gliderType" placeholder="Uwagi" maxlength="171" style="margin-bottom:10px;" id="commentsForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'comments\','+flight.id+',\'commentsCell'+flight.id+'\')">'+
			'</td></tr></a>');}
			else{
				$('#flightsList').append('<tr class="info" style="font-weight: bold;"><td style="vertical-align:middle;"><a href="flightdetails.html?id=' + flight.id + '&airportName='+airportName+'&dbCreated='+dbCreated+'">' + 
				flight.date +'</a></td><td style="vertical-align:middle;" class="rowlink-skip" id="pilotNameCell'+flight.id+'"><input type="text" class="form-control" placeholder="Pilot" maxlength="50" size="1" style="margin-bottom:10px;" id="pilotNameForm'+flight.id+'" onkeyup="autoWidth(id)" onblur="makeQueryToUpdateFlight(id, \'pilotName\','+flight.id+',\'pilotNameCell'+flight.id+'\')">'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="passengerNameCell'+flight.id+'"><input type="text" class="form-control" placeholder="Instruktor" maxlength="50" style="margin-bottom:10px;" name="pass" id="passengerNameForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'passengerName\','+flight.id+',\'passengerNameCell'+flight.id+'\')">'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="planePilotNameCell'+flight.id+'"><input type="text" class="form-control" placeholder="Pilot samolotu holującego" maxlength="50" style="margin-bottom:10px;" id="planePilotNameForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'planePilotName\','+flight.id+',\'planePilotNameCell'+flight.id+'\')">'+
				//'</td><td style="vertical-align:middle;" class="rowlink-skip" id="taskNumberCell'+flight.id+'"><input type="text" class="form-control" placeholder="Numer zadania" maxlength="15" style="margin-bottom:10px;" id="taskNumberForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'taskNumber\','+flight.id+',\'taskNumberCell'+flight.id+'\')">'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="exerciseNumberCell'+flight.id+'"><select class="form-control" id="exerciseNumberForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'exerciseNumber\','+flight.id+',\'exerciseNumberCell'+flight.id+'\')"><option>4 - Wstępny lot zapoznawczy</option><option>5 - Działanie układów sterowania</option><option>6 - Koordynowanie wykonywanie zakrętów ze średnim przechyleniem</option><option>7 - Lot po prostej</option><option>8 - Zakręty</option><option>9a - Lot na małej prędkości</option><option>9b - Przeciągnięcie</option><option>10a - Rozpoznanie korkociągu i zapobieganie wejściu  w korkociąg</option><option>10b - Korkociągi ustalone: wejście i  wyprowadzanie</option><option>11 - Metody startu</option><option>11a - Start za wyciągarką</option><option>11b - Start za samolotem holującym</option><option>11c - Start z własnym zespołem napędowym</option><option>11d - Start za samochodem holującym</option><option>11e - Start z lin gumowych</option><option>12 - Krąg, podejście do lądowania i  lądowanie</option><option>13 - Pierwszy samodzielny lot</option><option>14 - Głębokie zakręty</option><option>15 - Techniki szybowania</option><option>15a - Lot w prądzie termicznym</option><option>15b - Lot  żaglowy</option><option>15c - Lot falowy</option><option>16 - Lądowania w terenie przygodnym</option><option>17 - Lot nawigacyjny</option><option>17 - Lot nawigacyjny (samodzielny)</option><option>17a - Planowanie lotu</option><option>17b - Nawigacja w locie</option><option>17c - Techniki lotu nawigacyjnego</option><option>Inne</option></select>'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="startTimeCell'+flight.id+'"><a onclick="setTime(this.id,'+flight.id+',\'startTime\');" class="btn btn-info btn-lg" id="startTimeButton'+flight.id+'"  role="button"><span class="glyphicon glyphicon-time"></span></a>'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="disconnectionTimeCell'+flight.id+'"><a onclick="setTime(this.id,'+flight.id+',\'disconnectionTime\');" class="btn btn-danger btn-lg" id="disconnectionTimeButton'+flight.id+'"  role="button"><span class="glyphicon glyphicon-time"></span></a>'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="landingPlaneTimeCell'+flight.id+'"><a onclick="setTime(this.id,'+flight.id+',\'landingPlaneTime\');" class="btn btn-success btn-lg" id="landingPlaneTimeButton'+flight.id+'"  role="button"><span class="glyphicon glyphicon-time"></span></a>'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="landingGliderTimeCell'+flight.id+'"><a onclick="setTime(this.id,'+flight.id+',\'landingGliderTime\',\''+flight.startTime+'\');" class="btn btn-primary btn-lg" id="landingGliderTimeButton'+flight.id+'" role="button"><span class="glyphicon glyphicon-time"></span></a>'+
				'</td><td style="vertical-align:middle;" id="planeHourTimeCell'+flight.id+'">'+
				flight.planeHourTime+'</td><td style="vertical-align:middle;" id="planeMinTimeCell'+flight.id+'">'+
				flight.planeMinTime+'</td><td style="vertical-align:middle;" id="planeOnEarthTimeCell'+flight.id+'"> '+
				flight.planeOnEarthTime+'</td><td style="vertical-align:middle;">'+
				flight.gliderHourTime+'</td><td style="vertical-align:middle;">'+
				flight.gliderMinTime+'</td><td style="vertical-align:middle;" class="rowlink-skip" id="planeTypeCell'+flight.id+'"><input type="text" class="form-control" placeholder="Typ i oznaczenie samolotu" maxlength="15" style="margin-bottom:10px;" id="planeTypeForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'planeType\','+flight.id+',\'planeTypeCell'+flight.id+'\')">'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="gliderTypeCell'+flight.id+'"><input type="text" class="form-control gliderType" placeholder="Typ i oznaczenie szybowca" maxlength="15" style="margin-bottom:10px;" id="gliderTypeForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'gliderType\','+flight.id+',\'gliderTypeCell'+flight.id+'\')">'+
				'</td><td style="vertical-align:middle;" class="rowlink-skip" id="commentsCell'+flight.id+'"><input type="text" class="form-control gliderType" placeholder="Uwagi" maxlength="171" style="margin-bottom:10px;" id="commentsForm'+flight.id+'" onblur="makeQueryToUpdateFlight(id, \'comments\','+flight.id+',\'commentsCell'+flight.id+'\')">'+
				'</td></tr></a>');
			}
			if(flight.startTime!=''){
				var startTime = flight.startTime.toString();
				$('#startTimeCell'+flight.id).append(startTime.substring(0,5));
				$('#startTimeButton'+flight.id).hide();
				$('#startTimeCell'+flight.id).removeClass("rowlink-skip");
			}
			if(flight.disconnectionTime!=''){
				var disconnectionTime = flight.disconnectionTime.toString();
				$('#disconnectionTimeCell'+flight.id).append(disconnectionTime.substring(0,5));
				$('#disconnectionTimeButton'+flight.id).hide();
				$('#disconnectionTimeCell'+flight.id).removeClass("rowlink-skip");
			}
			if(flight.landingPlaneTime!=''){
				var landingPlaneTime = flight.landingPlaneTime.toString();
				$('#landingPlaneTimeCell'+flight.id).append(landingPlaneTime.substring(0,5));
				$('#landingPlaneTimeButton'+flight.id).hide();
				$('#landingPlaneTimeCell'+flight.id).removeClass("rowlink-skip");
			}
			if(flight.landingGliderTime!=''){
				var landingGliderTime = flight.landingGliderTime.toString();
				$('#landingGliderTimeCell'+flight.id).append(landingGliderTime.substring(0,5));
				$('#landingGliderTimeButton'+flight.id).hide();
				$('#landingGliderTimeCell'+flight.id).removeClass("rowlink-skip");
				//$('#pilotNameCell'+flight.id).append(flight.pilotName);
				$('#pilotNameForm'+flight.id).hide();
				$('#pilotNameCell'+flight.id).removeClass("rowlink-skip");
				//$('#passengerNameCell'+flight.id).append(flight.passengerName);
				$('#passengerNameForm'+flight.id).hide();
				$('#passengerNameCell'+flight.id).removeClass("rowlink-skip");
				//$('#startTimeCell'+flight.id).append(flight.startTime);
				$('#startTimeButton'+flight.id).hide();
				$('#startTimeCell'+flight.id).removeClass("rowlink-skip");
				//$('#disconnectionTimeCell'+flight.id).append(flight.disconnectionTime);
				$('#disconnectionTimeButton'+flight.id).hide();
				$('#disconnectionTimeCell'+flight.id).removeClass("rowlink-skip");
				//$('#planePilotNameCell'+flight.id).append(flight.planePilotName);
				$('#planePilotNameForm'+flight.id).hide();
				$('#planePilotNameCell'+flight.id).removeClass("rowlink-skip");
				//$('#gliderTypeCell'+flight.id).append(flight.gliderType);
				$('#gliderTypeForm'+flight.id).hide();
				$('#gliderTypeCell'+flight.id).removeClass("rowlink-skip");
				//$('#planeTypeCell'+flight.id).append(flight.planeType);
				$('#planeTypeForm'+flight.id).hide();
				$('#planeTypeCell'+flight.id).removeClass("rowlink-skip");
				//$('#taskNumberCell'+flight.id).append(flight.taskNumber);
				$('#taskNumberForm'+flight.id).hide();
				$('#taskNumberCell'+flight.id).removeClass("rowlink-skip");
				//$('#exerciseNumberCell'+flight.id).append(flight.exerciseNumber);
				$('#exerciseNumberForm'+flight.id).hide();
				$('#exerciseNumberCell'+flight.id).removeClass("rowlink-skip");
				//$('#commentsCell'+flight.id).append(flight.comments);
				$('#commentsForm'+flight.id).hide();
				$('#commentsCell'+flight.id).removeClass("rowlink-skip");
				//$('#landingPlaneTimeCell'+flight.id).append(flight.landingPlaneTime);
				$('#landingPlaneTimeButton'+flight.id).hide();
				$('#landingPlaneTimeCell'+flight.id).removeClass("rowlink-skip");
				if(flight.planePilotName==''){
					$('#planePilotNameCell'+flight.id).append('X');
					$('#planeTypeCell'+flight.id).append('X');
					$('#landingPlaneTimeCell'+flight.id).append('X');
					$('#planeMinTimeCell'+flight.id).append('X');
					$('#planeOnEarthTimeCell'+flight.id).append('X');
					$('#planeHourTimeCell'+flight.id).append('X');
				}
			}	
			if(flight.pilotName!=''){
				$('#pilotNameCell'+flight.id).append(flight.pilotName);
				$('#pilotNameForm'+flight.id).hide();
				$('#pilotNameCell'+flight.id).removeClass("rowlink-skip");
			}
			if(flight.passengerName!=''){
				$('#passengerNameCell'+flight.id).append(flight.passengerName);
				$('#passengerNameForm'+flight.id).hide();
				$('#passengerNameCell'+flight.id).removeClass("rowlink-skip");
			}	
			if(flight.planePilotName!=''){
				$('#planePilotNameCell'+flight.id).append(flight.planePilotName);
				$('#planePilotNameForm'+flight.id).hide();
				$('#planePilotNameCell'+flight.id).removeClass("rowlink-skip");
			}	
			if(flight.gliderType!=''){
				$('#gliderTypeCell'+flight.id).append(flight.gliderType);
				$('#gliderTypeForm'+flight.id).hide();
				$('#gliderTypeCell'+flight.id).removeClass("rowlink-skip");
			}
			if(flight.planeType!=''){
				$('#planeTypeCell'+flight.id).append(flight.planeType);
				$('#planeTypeForm'+flight.id).hide();
				$('#planeTypeCell'+flight.id).removeClass("rowlink-skip");
			}
			if(flight.taskNumber!=''){
				$('#taskNumberCell'+flight.id).append(flight.taskNumber);
				$('#taskNumberForm'+flight.id).hide();
				$('#taskNumberCell'+flight.id).removeClass("rowlink-skip");
			}
			if(flight.exerciseNumber!=''){
				$('#exerciseNumberCell'+flight.id).append(flight.exerciseNumber);
				$('#exerciseNumberForm'+flight.id).hide();
				$('#exerciseNumberCell'+flight.id).removeClass("rowlink-skip");
			}
			if(flight.comments!=''){
				$('#commentsCell'+flight.id).append(flight.comments);
				$('#commentsForm'+flight.id).hide();
				$('#commentsCell'+flight.id).removeClass("rowlink-skip");
			}
		}
	}
	db = null;
}

function makeQueryToUpdateFlight(inputField, databaseColumn, flightID,cell) {
	window.flightID=flightID;
	window.inputField=inputField;
	window.databaseColumn=databaseColumn;
	db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
	db.transaction(updateFlight, transaction_error);
	if(inputField.substring(0,18)=='exerciseNumberForm'){
		var exercise = document.getElementById(inputField).value.toString();
		exerciseType=exercise.split(" ");
		//alert("Kod: "+exerciseType[0]);
		$('#'+cell).append(exerciseType[0]);
	}
	else
		$('#'+cell).append(document.getElementById(inputField).value);
	if(document.getElementById(inputField).value!='')
		$('#'+inputField).hide();
	
}

function updateFlight(tx){
	var updateFlightQuery = "UPDATE flights SET "+databaseColumn+"='"+document.getElementById(inputField).value+"' WHERE id="+flightID;
	
	if(inputField.substring(0,18)=='exerciseNumberForm'){
		var exercise = document.getElementById(inputField).value.toString();
		exerciseType=exercise.split(" ");
		//alert("Kod: "+exerciseType[0]);
		var updateFlightQuery = "UPDATE flights SET "+databaseColumn+"='"+exerciseType[0]+"' WHERE id="+flightID;
	}
	if(databaseColumn=='planePilotName' && document.getElementById(inputField).value!='')
		var updateFlightQuery = "UPDATE flights SET "+databaseColumn+"='"+document.getElementById(inputField).value+"', startMode='S' WHERE id="+flightID;
	tx.executeSql(updateFlightQuery);
}



function populateDB(tx) {
	tx.executeSql('DROP TABLE IF EXISTS flights');
	var sql = 
		"CREATE TABLE IF NOT EXISTS flights ( "+
		"id INTEGER PRIMARY KEY AUTOINCREMENT, " +
		"date VARCHAR(10), " +
		"pilotName VARCHAR(50), " +
		"planePilotName VARCHAR(50), " +
		"passengerName VARCHAR(50), " +
		"taskNumber VARCHAR(15), " +
		"exerciseNumber VARCHAR(50), " + 
		"startMode VARCHAR(5), " +
		"startTime VARCHAR(8), " +
		"disconnectionTime VARCHAR(8), " + 
		"landingGliderTime VARCHAR(8), " +
		"landingPlaneTime VARCHAR(8), " +
		"gliderHourTime VARCHAR(5), " +
		"gliderMinTime VARCHAR(5), " +
		"planeOnEarthTime VARCHAR(5), " +
		"planeHourTime VARCHAR(5), " +
		"planeMinTime VARCHAR(5), " +
		"gliderType VARCHAR(20), " +
		"planeType VARCHAR(20), " +
		"status VARCHAR(25), " +
		"comments VARCHAR(200))";
    tx.executeSql(sql);
}


function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}