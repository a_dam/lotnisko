var id = getUrlVars()["id"];
var airportName = getUrlVars()["airportName"];
var dbCreated = getUrlVars()["dbCreated"];
var db;

onDeviceReady();
function onDeviceReady() {
	console.log("opening database");
    db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
	console.log("database opened");
    db.transaction(getFlight, transaction_error);
}

$(document).ready(function(){
	$("#flightListButton").click(function(){
      window.location.href="index.html?dbCreated="+dbCreated+"&airportName="+airportName;
    });
	$("#editButton").click(function(){
	if((document.getElementById("startTimeForm").value!='' && checkTime(startTimeForm)==false) || (document.getElementById("disconnectionTimeForm").value!='' && checkTime(disconnectionTimeForm)==false) || (document.getElementById("landingPlaneTimeForm").value!='' && checkTime(landingPlaneTimeForm)==false) || (document.getElementById("landingGliderTimeForm").value!='' && checkTime(landingGliderTimeForm)==false)){
			$('#hourTimeBadFormatAlert').show();
		}	
		$('#dateEditPanel').show();
		$('#passengerNamePanel').show();
		$('#pilotNamePanel').show();
		$('#planePilotNamePanel').show();
		$('#flightModeEditPanel').show();
		$('#startTimePanel').show();
		$('#disconnectionTimePanel').show();
		$('#landingPlaneTimePanel').show();
		$('#landingGliderTimePanel').show();
		$('#planeHourTimeForm').show();
		$('#planeMinTimeForm').show();
		$('#planeOnEarthTimeForm').show();
		$('#gliderHourTimeForm').show();
		$('#gliderMinTimeForm').show();
		$('#planeTypeEditPanel').show();
		$('#gliderTypeEditPanel').show();
		$('#commentsEditPanel').show();
		$('#navButtons').show();
		$('#editButton').hide();
		if(document.getElementById("startTimeForm").value=='')
			$('#setStartTimeButton').show();
		else
			$('#startTimeForm').show();
		if(document.getElementById("disconnectionTimeForm").value=='')
			$('#setDisconnectionTimeButton').show();
		else
			$('#disconnectionTimeForm').show();
		if(document.getElementById("landingGliderTimeForm").value=='')
			$('#setLandingGliderTimeButton').show();
		else
			$('#landingGliderTimeForm').show();
		if(document.getElementById("landingPlaneTimeForm").value=='')
			$('#setLandingPlaneTimeButton').show();
		else
			$('#landingPlaneTimeForm').show();	
		db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
		db.transaction(getNames, transaction_error);
		db.transaction(getPlanes, transaction_error);	
		db.transaction(getGliders, transaction_error);
	});
	$("#cancelChanges").click(function(){
		$('#dateEditPanel').hide();
		$('#passengerNamePanel').hide();
		$('#pilotNamePanel').hide();
		$('#planePilotNamePanel').hide();
		$('#flightModeEditPanel').hide();
		$('#startTimePanel').hide();
		$('#disconnectionTimePanel').hide();
		$('#landingPlaneTimePanel').hide();
		$('#landingGliderTimePanel').hide();
		$('#planeTypeEditPanel').hide();
		$('#gliderTypeEditPanel').hide();
		$('#commentsEditPanel').hide();
		$('#navButtons').hide();
		$('#editButton').show();
		$('#hourTimeBadFormatAlert').hide();
		$('#cancelModal').modal('hide');
    });
	$("#saveChanges").click(function(){
		checkSeconds(startTimeForm);
		checkSeconds(disconnectionTimeForm);
		checkSeconds(landingGliderTimeForm);
		checkSeconds(landingPlaneTimeForm);
		if((document.getElementById("startTimeForm").value!='' && checkTime(startTimeForm)==false) || (document.getElementById("disconnectionTimeForm").value!='' && checkTime(disconnectionTimeForm)==false) || (document.getElementById("landingPlaneTimeForm").value!='' && checkTime(landingPlaneTimeForm)==false) || (document.getElementById("landingGliderTimeForm").value!='' && checkTime(landingGliderTimeForm)==false)){
			$('#hourTimeBadFormatAlert').show();
			null.dummy;
		}
		else {
			if(document.getElementById("startTimeForm").value!='' && document.getElementById("landingGliderTimeForm").value!=''){	
				document.getElementById("gliderHourTimeForm").value = timeDifference(startTimeForm.value,landingGliderTimeForm.value,'H');
				document.getElementById("gliderMinTimeForm").value = timeDifference(startTimeForm.value,landingGliderTimeForm.value,'M');
			}
			if(document.getElementById("startTimeForm").value!='' && document.getElementById("landingPlaneTimeForm").value!=''){	
				document.getElementById("planeHourTimeForm").value = timeDifference(startTimeForm.value,landingPlaneTimeForm.value,'H');
				document.getElementById("planeMinTimeForm").value = timeDifference(startTimeForm.value,landingPlaneTimeForm.value,'M');
			}
		}
		db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
		db.transaction(editFlight, transaction_error);
		window.location.href="index.html?dbCreated="+dbCreated;
	});
	$("#setDateButton").click(function(){
		getDate();
		$('#navButtons').show();
	});
	$("#setStartTimeButton").click(function(){
		getTime(0);
		$('#navButtons').show();
		$('#setStartTimeButton').hide();
	});
	$("#setDisconnectionTimeButton").click(function(){
		getTime(1);
		$('#navButtons').show();
		$('#setDisconnectionTimeButton').hide();
	});
	$("#setLandingPlaneTimeButton").click(function(){
		getTime(2);
		$('#navButtons').show();
		$('#setLandingPlaneTimeButton').hide();
	});
	$("#setLandingGliderTimeButton").click(function(){
		getTime(3);		
		$('#navButtons').show();
		$('#setLandingGliderTimeButton').hide();
	});
	$("#confirmRemoveFlights").click(function(){
		db = window.openDatabase("FlightsDB", "1.0", "PhoneGap Demo", 200000);
		db.transaction(removeFlight, transaction_error);
		window.location.href="index.html?dbCreated="+dbCreated;
	});
	$("#pilotNameForm,#passengerNameForm,#planePilotNameForm,#gliderTypeForm,#planeTypeForm").bind('focus', function(){ $(this).autocomplete("search"); } );
})

function transaction_error(tx, error) {
    console.log("[Szczegóły] Błąd bazy danych " + error);
}

function getFlight(tx) {
	var sql = "SELECT * " +
			  "FROM flights " +
			  "WHERE id=:id";
	tx.executeSql(sql, [id], getFlight_success);
}

function editFlight(tx){
	if(document.getElementById("landingGliderTimeForm").value=='')
		status = 'W powietrzu';
	else
		status = 'Zakonczony';
	var updateQuery = "UPDATE flights " +
			  "SET date='"+ document.getElementById("dateForm").value +"', pilotName='"+ document.getElementById("pilotNameForm").value +"', passengerName='"+document.getElementById("passengerNameForm").value+"', planePilotName='"+ document.getElementById("planePilotNameForm").value +"', taskNumber='"+ document.getElementById("taskNumberForm").value +"', exerciseNumber='"+ document.getElementById("exerciseNumberForm").value +"', startTime='"+ document.getElementById("startTimeForm").value +"', disconnectionTime='"+ document.getElementById("disconnectionTimeForm").value +"', landingPlaneTime='"+ document.getElementById("landingPlaneTimeForm").value +"', landingGliderTime='"+ document.getElementById("landingGliderTimeForm").value +"', planeHourTime='"+ document.getElementById("planeHourTimeForm").value +"', planeMinTime='"+ document.getElementById("planeMinTimeForm").value +"', planeOnEarthTime='"+ document.getElementById("planeOnEarthTimeForm").value +"', gliderHourTime='"+ document.getElementById("gliderHourTimeForm").value +"', gliderMinTime='"+ document.getElementById("gliderMinTimeForm").value +"', planeType='"+ document.getElementById("planeTypeForm").value +"', gliderType='"+ document.getElementById("gliderTypeForm").value +"', comments='"+ document.getElementById("commentsForm").value +"', status='"+status+"' "+
			  "WHERE id="+id;
	tx.executeSql(updateQuery);
}

function removeFlight(tx){
	var removeQuery = "DELETE FROM flights WHERE id="+id;
	tx.executeSql(removeQuery);
}

function getDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;

	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd}
	if(mm<10){mm='0'+mm}
	today = yyyy+'.'+mm+'.'+dd;
	$('#dateForm').show();
	document.getElementById('dateForm').value = today;
}

function getTime(field){
	var today = new Date();
	var hours  = today.getHours();
	var mins = today.getMinutes();
	var seconds = today.getSeconds();
	hoursString = hours.toString();
	minsString = mins.toString();
	secondsString = seconds.toString();
	if(hoursString.length==1)
		hoursString='0'+hoursString;
	if(minsString.length==1)
		minsString='0'+minsString;
	if(secondsString.length==1)
		secondsString='0'+secondsString;
	
	time = hoursString+':'+minsString+':'+secondsString;
	
	if(field==0){
		$('#startTimeForm').show();
		document.getElementById('startTimeForm').value = time;
	}
	if(field==1){
		$('#disconnectionTimeForm').show();
		document.getElementById('disconnectionTimeForm').value = time;
	}
	if(field==2){
		$('#landingPlaneTimeForm').show();
		document.getElementById('landingPlaneTimeForm').value = time;
	}
	if(field==3){
		$('#landingGliderTimeForm').show();
		document.getElementById('landingGliderTimeForm').value = time;
	}
}



function checkTime(field) {
	var timeReg = new RegExp('^[0-9]{2}:[0-9]{2}(:[0-9]{2})?$', 'gi');
	if(!timeReg.test(field.value)){
		return false;
	} else {
		return true;
	}
}

function checkDate(field) {
	var dateReg = new RegExp('^[0-9]{2}.[0-9]{2}.[0-9]{4}$', 'gi');
	if(!dateReg.test(field.value)){
		return false;
	} else {
		return true;
	}
}

function checkSeconds(field) {
	var time = field.value;
	if(time.length==5){
		time=time+':00';
		field.value=time;
	}
}

function timeDifference(time1, time2, timePart) {
   var parts = time1.split(':');
   var date1 = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
   parts = time2.split(':');
   var date2 = new Date(new Date(0, 0, 0, parts[0], parts[1], parts[2]) - date1);

	if(timePart=='H')
		return (date2.getHours()-1);
	if(timePart=='M'){
		return (date2.getMinutes());
	}
}

function getFlight_success(tx, results) {
	var flight = results.rows.item(0);
	$('#dateLabel').append(flight.date+"<br><strong>Na ziemi: </strong>"+flight.planeOnEarthTime);
	$('#dateForm').val(flight.date);
	//$('#planeOnEarthTimeForm').val(flight.date);
	$('#pilotNameLabel').append(flight.pilotName);
	$('#pilotNameForm').val(flight.pilotName);
	$('#passengerNameLabel').append(flight.passengerName);
	$('#passengerNameForm').val(flight.passengerName);
	$('#planePilotNameLabel').append(flight.planePilotName);
	$('#planePilotNameForm').val(flight.planePilotName);
	$('#taskNumberLabel').append(flight.taskNumber);
	$('#taskNumberForm').val(flight.taskNumber);
	$('#flightModeLabel').append('Zadanie: '+flight.exerciseNumber+'<br>Ćwiczenie: '+flight.taskNumber);
	$('#exerciseNumberLabel').append(flight.exerciseNumber);
	$('#exerciseNumberForm').val(flight.exerciseNumber);
	$('#startTimeLabel').append(flight.startTime);
	$('#startTimeForm').val(flight.startTime);
	$('#disconnectionTimeLabel').append(flight.disconnectionTime);
	$('#disconnectionTimeForm').val(flight.disconnectionTime);
	$('#landingPlaneTimeLabel').append(flight.landingPlaneTime);
	$('#landingPlaneTimeForm').val(flight.landingPlaneTime);
	$('#landingGliderTimeLabel').append(flight.landingGliderTime);
	$('#landingGliderTimeForm').val(flight.landingGliderTime);
	$('#planeHourTimeLabel').text(flight.planeHourTime);
	$('#planeHourTimeForm').val(flight.planeHourTime);
	$('#planeMinTimeLabel').text(flight.planeMinTime);
	$('#planeMinTimeForm').val(flight.planeMinTime);
	$('#planeOnEarthTimeLabel').text(flight.planeOnEarthTime);
	$('#planeOnEarthTimeForm').val(flight.planeOnEarthTime);
	$('#gliderHourTimeLabel').text(flight.gliderHourTime);
	$('#gliderHourTimeForm').val(flight.gliderHourTime);
	$('#gliderMinTimeLabel').text(flight.gliderMinTime);
	$('#gliderMinTimeForm').val(flight.gliderMinTime);
	$('#planeTypeLabel').append(flight.planeType);
	$('#planeTypeForm').val(flight.planeType);
	$('#gliderTypeLabel').append(flight.gliderType);
	$('#gliderTypeForm').val(flight.gliderType);
	$('#commentsLabel').append(flight.comments);
	$('#commentsForm').val(flight.comments);
	$('#statusForm').val(flight.status);
	db = null;
}

function getNames(tx) {
	var getNamesQuery = "SELECT pilotName FROM flights UNION SELECT passengerName FROM flights UNION SELECT planePilotName FROM flights";
	tx.executeSql(getNamesQuery,[], getNames_success);
}

function getNames_success(tx,results) {
	window.results = results;
	var len = results.rows.length;
	window.names = [];	
	for (var i=0; i<len; i++) {
		var flightName = results.rows.item(i);
		window.names.push(flightName.pilotName);
	}
	$("#pilotNameForm,#passengerNameForm,#planePilotNameForm").autocomplete({
		  source: window.names,
		  minLength: 0
		});
}

function getGliders(tx) {
	var getGliders = "SELECT DISTINCT gliderType FROM flights";
	tx.executeSql(getGliders,[], getGliders_success);
}

function getGliders_success(tx,results) {
	window.results = results;
	var len = results.rows.length;
	window.gliders = [];	
	for (var i=0; i<len; i++) {
		var flightGlider = results.rows.item(i);
		window.gliders.push(flightGlider.gliderType);
	}
	$("#gliderTypeForm").autocomplete({
		  source: window.gliders,
		  minLength: 0
		});
	//$("#gliderTypeForm").autocomplete( "option", "appendTo", ".eventInsForm" );
}

function getPlanes(tx) {
	var getPlanes = "SELECT DISTINCT planeType FROM flights";
	tx.executeSql(getPlanes,[], getPlanes_success);
}

function getPlanes_success(tx,results) {
	window.results = results;
	var len = results.rows.length;
	window.planes = [];	
	for (var i=0; i<len; i++) {
		var flightPlane = results.rows.item(i);
		window.planes.push(flightPlane.planeType);
	}
	$("#planeTypeForm").autocomplete({
		  source: window.planes,
		  minLength: 0
		});
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}